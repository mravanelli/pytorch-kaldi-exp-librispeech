ep=000 tr=['train_clean_100'] loss=1.797 err=0.477 valid=dev_clean loss=1.449 err=0.408 lr_architecture1=0.080000 time(s)=4535
ep=001 tr=['train_clean_100'] loss=1.490 err=0.419 valid=dev_clean loss=1.360 err=0.388 lr_architecture1=0.080000 time(s)=4417
ep=002 tr=['train_clean_100'] loss=1.428 err=0.405 valid=dev_clean loss=1.323 err=0.379 lr_architecture1=0.080000 time(s)=4291
ep=003 tr=['train_clean_100'] loss=1.394 err=0.398 valid=dev_clean loss=1.297 err=0.373 lr_architecture1=0.080000 time(s)=4513
ep=004 tr=['train_clean_100'] loss=1.372 err=0.393 valid=dev_clean loss=1.276 err=0.369 lr_architecture1=0.080000 time(s)=4426
ep=005 tr=['train_clean_100'] loss=1.356 err=0.389 valid=dev_clean loss=1.263 err=0.365 lr_architecture1=0.080000 time(s)=4434
ep=006 tr=['train_clean_100'] loss=1.343 err=0.386 valid=dev_clean loss=1.254 err=0.364 lr_architecture1=0.080000 time(s)=4373
ep=007 tr=['train_clean_100'] loss=1.333 err=0.384 valid=dev_clean loss=1.249 err=0.362 lr_architecture1=0.080000 time(s)=4407
ep=008 tr=['train_clean_100'] loss=1.325 err=0.382 valid=dev_clean loss=1.241 err=0.359 lr_architecture1=0.080000 time(s)=4525
ep=009 tr=['train_clean_100'] loss=1.317 err=0.381 valid=dev_clean loss=1.231 err=0.358 lr_architecture1=0.080000 time(s)=4623
ep=010 tr=['train_clean_100'] loss=1.311 err=0.379 valid=dev_clean loss=1.225 err=0.356 lr_architecture1=0.080000 time(s)=4599
ep=011 tr=['train_clean_100'] loss=1.305 err=0.378 valid=dev_clean loss=1.220 err=0.355 lr_architecture1=0.080000 time(s)=4679
ep=012 tr=['train_clean_100'] loss=1.301 err=0.377 valid=dev_clean loss=1.221 err=0.355 lr_architecture1=0.080000 time(s)=5260
ep=013 tr=['train_clean_100'] loss=1.296 err=0.376 valid=dev_clean loss=1.214 err=0.353 lr_architecture1=0.080000 time(s)=5807
ep=014 tr=['train_clean_100'] loss=1.292 err=0.375 valid=dev_clean loss=1.211 err=0.353 lr_architecture1=0.080000 time(s)=5497
ep=015 tr=['train_clean_100'] loss=1.278 err=0.371 valid=dev_clean loss=1.188 err=0.347 lr_architecture1=0.040000 time(s)=5109
ep=016 tr=['train_clean_100'] loss=1.272 err=0.370 valid=dev_clean loss=1.187 err=0.346 lr_architecture1=0.040000 time(s)=5008
ep=017 tr=['train_clean_100'] loss=1.270 err=0.369 valid=dev_clean loss=1.185 err=0.346 lr_architecture1=0.040000 time(s)=4884
ep=018 tr=['train_clean_100'] loss=1.263 err=0.367 valid=dev_clean loss=1.172 err=0.343 lr_architecture1=0.020000 time(s)=4893
ep=019 tr=['train_clean_100'] loss=1.260 err=0.367 valid=dev_clean loss=1.172 err=0.343 lr_architecture1=0.020000 time(s)=4733
ep=020 tr=['train_clean_100'] loss=1.257 err=0.366 valid=dev_clean loss=1.164 err=0.341 lr_architecture1=0.010000 time(s)=4780
ep=021 tr=['train_clean_100'] loss=1.255 err=0.365 valid=dev_clean loss=1.166 err=0.341 lr_architecture1=0.010000 time(s)=4611
ep=022 tr=['train_clean_100'] loss=1.253 err=0.365 valid=dev_clean loss=1.159 err=0.340 lr_architecture1=0.005000 time(s)=4829
ep=023 tr=['train_clean_100'] loss=1.252 err=0.365 valid=dev_clean loss=1.160 err=0.340 lr_architecture1=0.005000 time(s)=4610
%WER 9.62 [ 5060 / 52576, 577 ins, 612 del, 3871 sub ] /scratch/ravanelm/pytorch-kaldi-new/exp/libri_fmllr/decode_test_clean_out_dnn1/wer_12_0.0

