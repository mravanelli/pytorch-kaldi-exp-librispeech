ep=000 tr=['train_clean_100'] loss=1.636 err=0.422 valid=dev_clean loss=0.990 err=0.285 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=9741
ep=001 tr=['train_clean_100'] loss=1.046 err=0.302 valid=dev_clean loss=0.858 err=0.253 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=10772
ep=002 tr=['train_clean_100'] loss=0.906 err=0.268 valid=dev_clean loss=0.788 err=0.236 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=13600
ep=003 tr=['train_clean_100'] loss=0.844 err=0.252 valid=dev_clean loss=0.743 err=0.224 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=15142
ep=004 tr=['train_clean_100'] loss=0.808 err=0.243 valid=dev_clean loss=0.724 err=0.218 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=15047
ep=005 tr=['train_clean_100'] loss=0.780 err=0.237 valid=dev_clean loss=0.714 err=0.216 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=15272
ep=006 tr=['train_clean_100'] loss=0.759 err=0.231 valid=dev_clean loss=0.699 err=0.211 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=14809
ep=007 tr=['train_clean_100'] loss=0.741 err=0.227 valid=dev_clean loss=0.687 err=0.208 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=14997
ep=008 tr=['train_clean_100'] loss=0.727 err=0.223 valid=dev_clean loss=0.680 err=0.206 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=14859
ep=009 tr=['train_clean_100'] loss=0.714 err=0.220 valid=dev_clean loss=0.674 err=0.204 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=14764
ep=010 tr=['train_clean_100'] loss=0.703 err=0.217 valid=dev_clean loss=0.663 err=0.201 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=15132
ep=011 tr=['train_clean_100'] loss=0.693 err=0.215 valid=dev_clean loss=0.665 err=0.201 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=15342
ep=012 tr=['train_clean_100'] loss=0.684 err=0.212 valid=dev_clean loss=0.667 err=0.201 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=15798
ep=013 tr=['train_clean_100'] loss=0.659 err=0.206 valid=dev_clean loss=0.656 err=0.197 lr_architecture1=0.000100 lr_architecture2=0.000100 time(s)=15499
ep=014 tr=['train_clean_100'] loss=0.650 err=0.204 valid=dev_clean loss=0.655 err=0.197 lr_architecture1=0.000100 lr_architecture2=0.000100 time(s)=15300
ep=015 tr=['train_clean_100'] loss=0.645 err=0.202 valid=dev_clean loss=0.653 err=0.196 lr_architecture1=0.000100 lr_architecture2=0.000100 time(s)=15764
ep=016 tr=['train_clean_100'] loss=0.641 err=0.201 valid=dev_clean loss=0.651 err=0.196 lr_architecture1=0.000100 lr_architecture2=0.000100 time(s)=16385
ep=017 tr=['train_clean_100'] loss=0.637 err=0.200 valid=dev_clean loss=0.656 err=0.195 lr_architecture1=0.000100 lr_architecture2=0.000100 time(s)=16831
ep=018 tr=['train_clean_100'] loss=0.633 err=0.199 valid=dev_clean loss=0.657 err=0.195 lr_architecture1=0.000100 lr_architecture2=0.000100 time(s)=16592
ep=019 tr=['train_clean_100'] loss=0.621 err=0.196 valid=dev_clean loss=0.654 err=0.194 lr_architecture1=0.000050 lr_architecture2=0.000050 time(s)=16344
ep=020 tr=['train_clean_100'] loss=0.617 err=0.195 valid=dev_clean loss=0.648 err=0.193 lr_architecture1=0.000050 lr_architecture2=0.000050 time(s)=16441
ep=021 tr=['train_clean_100'] loss=0.615 err=0.194 valid=dev_clean loss=0.650 err=0.193 lr_architecture1=0.000050 lr_architecture2=0.000050 time(s)=16957
ep=022 tr=['train_clean_100'] loss=0.608 err=0.193 valid=dev_clean loss=0.648 err=0.192 lr_architecture1=0.000025 lr_architecture2=0.000025 time(s)=17495
ep=023 tr=['train_clean_100'] loss=0.606 err=0.192 valid=dev_clean loss=0.651 err=0.192 lr_architecture1=0.000025 lr_architecture2=0.000025 time(s)=17576
%WER 8.63 [ 4537 / 52576, 568 ins, 503 del, 3466 sub ] /scratch/ravanelm/pytorch-kaldi-new/exp/libri_liGRU_fmllr/decode_test_clean_out_dnn2/wer_15_0.0

