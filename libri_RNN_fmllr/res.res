ep=000 tr=['train_clean_100'] loss=1.976 err=0.484 valid=dev_clean loss=1.297 err=0.349 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=5394
ep=001 tr=['train_clean_100'] loss=1.364 err=0.369 valid=dev_clean loss=1.173 err=0.321 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=5679
ep=002 tr=['train_clean_100'] loss=1.211 err=0.335 valid=dev_clean loss=1.109 err=0.305 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=6495
ep=003 tr=['train_clean_100'] loss=1.142 err=0.318 valid=dev_clean loss=1.066 err=0.293 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=7005
ep=004 tr=['train_clean_100'] loss=1.105 err=0.310 valid=dev_clean loss=1.055 err=0.290 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=7007
ep=005 tr=['train_clean_100'] loss=1.077 err=0.303 valid=dev_clean loss=1.048 err=0.288 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=7222
ep=006 tr=['train_clean_100'] loss=1.056 err=0.299 valid=dev_clean loss=1.037 err=0.285 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=7207
ep=007 tr=['train_clean_100'] loss=1.041 err=0.295 valid=dev_clean loss=1.040 err=0.285 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=7156
ep=008 tr=['train_clean_100'] loss=1.025 err=0.291 valid=dev_clean loss=1.028 err=0.283 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=7174
ep=009 tr=['train_clean_100'] loss=1.014 err=0.289 valid=dev_clean loss=1.025 err=0.281 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=7151
ep=010 tr=['train_clean_100'] loss=1.003 err=0.286 valid=dev_clean loss=1.014 err=0.278 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=7912
ep=011 tr=['train_clean_100'] loss=0.995 err=0.284 valid=dev_clean loss=1.016 err=0.279 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=7608
ep=012 tr=['train_clean_100'] loss=0.956 err=0.275 valid=dev_clean loss=0.996 err=0.273 lr_architecture1=0.000160 lr_architecture2=0.000200 time(s)=7247
ep=013 tr=['train_clean_100'] loss=0.942 err=0.271 valid=dev_clean loss=0.992 err=0.273 lr_architecture1=0.000160 lr_architecture2=0.000200 time(s)=7126
ep=014 tr=['train_clean_100'] loss=0.921 err=0.267 valid=dev_clean loss=0.984 err=0.270 lr_architecture1=0.000080 lr_architecture2=0.000100 time(s)=7003
ep=015 tr=['train_clean_100'] loss=0.915 err=0.265 valid=dev_clean loss=0.978 err=0.268 lr_architecture1=0.000080 lr_architecture2=0.000100 time(s)=6996
ep=016 tr=['train_clean_100'] loss=0.908 err=0.263 valid=dev_clean loss=0.972 err=0.267 lr_architecture1=0.000080 lr_architecture2=0.000100 time(s)=6878
ep=017 tr=['train_clean_100'] loss=0.898 err=0.261 valid=dev_clean loss=0.970 err=0.268 lr_architecture1=0.000040 lr_architecture2=0.000050 time(s)=6880
ep=018 tr=['train_clean_100'] loss=0.893 err=0.260 valid=dev_clean loss=0.966 err=0.268 lr_architecture1=0.000020 lr_architecture2=0.000025 time(s)=6721
ep=019 tr=['train_clean_100'] loss=0.890 err=0.259 valid=dev_clean loss=0.967 err=0.267 lr_architecture1=0.000020 lr_architecture2=0.000025 time(s)=6651
ep=020 tr=['train_clean_100'] loss=0.889 err=0.259 valid=dev_clean loss=0.972 err=0.267 lr_architecture1=0.000020 lr_architecture2=0.000025 time(s)=7037
ep=021 tr=['train_clean_100'] loss=0.886 err=0.258 valid=dev_clean loss=0.968 err=0.267 lr_architecture1=0.000010 lr_architecture2=0.000013 time(s)=6907
ep=022 tr=['train_clean_100'] loss=0.885 err=0.258 valid=dev_clean loss=0.970 err=0.267 lr_architecture1=0.000005 lr_architecture2=0.000006 time(s)=6768
ep=023 tr=['train_clean_100'] loss=0.884 err=0.257 valid=dev_clean loss=0.966 err=0.267 lr_architecture1=0.000003 lr_architecture2=0.000003 time(s)=6842
%WER 9.90 [ 5205 / 52576, 672 ins, 522 del, 4011 sub ] /scratch/ravanelm/pytorch-kaldi-new/exp/libri_RNN_fmllr/decode_test_clean_out_dnn2/wer_11_0.0

