ep=000 tr=['train_clean_100'] loss=1.621 err=0.404 valid=dev_clean loss=1.055 err=0.283 lr_architecture1=0.001600 lr_architecture2=0.000400 time(s)=14331
ep=001 tr=['train_clean_100'] loss=1.020 err=0.289 valid=dev_clean loss=0.876 err=0.243 lr_architecture1=0.001600 lr_architecture2=0.000400 time(s)=15971
ep=002 tr=['train_clean_100'] loss=0.854 err=0.250 valid=dev_clean loss=0.786 err=0.224 lr_architecture1=0.001600 lr_architecture2=0.000400 time(s)=20830
ep=003 tr=['train_clean_100'] loss=0.780 err=0.232 valid=dev_clean loss=0.731 err=0.210 lr_architecture1=0.001600 lr_architecture2=0.000400 time(s)=24311
ep=004 tr=['train_clean_100'] loss=0.739 err=0.222 valid=dev_clean loss=0.713 err=0.205 lr_architecture1=0.001600 lr_architecture2=0.000400 time(s)=23716
ep=005 tr=['train_clean_100'] loss=0.710 err=0.215 valid=dev_clean loss=0.706 err=0.203 lr_architecture1=0.001600 lr_architecture2=0.000400 time(s)=23227
ep=006 tr=['train_clean_100'] loss=0.686 err=0.209 valid=dev_clean loss=0.688 err=0.197 lr_architecture1=0.001600 lr_architecture2=0.000400 time(s)=23105
ep=007 tr=['train_clean_100'] loss=0.670 err=0.205 valid=dev_clean loss=0.686 err=0.197 lr_architecture1=0.001600 lr_architecture2=0.000400 time(s)=23080
ep=008 tr=['train_clean_100'] loss=0.653 err=0.201 valid=dev_clean loss=0.673 err=0.192 lr_architecture1=0.001600 lr_architecture2=0.000400 time(s)=21565
ep=009 tr=['train_clean_100'] loss=0.639 err=0.198 valid=dev_clean loss=0.671 err=0.192 lr_architecture1=0.001600 lr_architecture2=0.000400 time(s)=21725
ep=010 tr=['train_clean_100'] loss=0.628 err=0.195 valid=dev_clean loss=0.663 err=0.190 lr_architecture1=0.001600 lr_architecture2=0.000400 time(s)=21402
ep=011 tr=['train_clean_100'] loss=0.618 err=0.192 valid=dev_clean loss=0.658 err=0.187 lr_architecture1=0.001600 lr_architecture2=0.000400 time(s)=21352
ep=012 tr=['train_clean_100'] loss=0.608 err=0.190 valid=dev_clean loss=0.661 err=0.187 lr_architecture1=0.001600 lr_architecture2=0.000400 time(s)=22800
ep=013 tr=['train_clean_100'] loss=0.600 err=0.188 valid=dev_clean loss=0.657 err=0.185 lr_architecture1=0.001600 lr_architecture2=0.000400 time(s)=21413
ep=014 tr=['train_clean_100'] loss=0.592 err=0.186 valid=dev_clean loss=0.661 err=0.186 lr_architecture1=0.001600 lr_architecture2=0.000400 time(s)=21631
ep=015 tr=['train_clean_100'] loss=0.558 err=0.177 valid=dev_clean loss=0.647 err=0.182 lr_architecture1=0.000800 lr_architecture2=0.000200 time(s)=21713
ep=016 tr=['train_clean_100'] loss=0.545 err=0.174 valid=dev_clean loss=0.646 err=0.180 lr_architecture1=0.000800 lr_architecture2=0.000200 time(s)=22920
ep=017 tr=['train_clean_100'] loss=0.537 err=0.171 valid=dev_clean loss=0.648 err=0.180 lr_architecture1=0.000800 lr_architecture2=0.000200 time(s)=22946
ep=018 tr=['train_clean_100'] loss=0.532 err=0.170 valid=dev_clean loss=0.647 err=0.178 lr_architecture1=0.000800 lr_architecture2=0.000200 time(s)=22382
ep=019 tr=['train_clean_100'] loss=0.527 err=0.169 valid=dev_clean loss=0.649 err=0.178 lr_architecture1=0.000800 lr_architecture2=0.000200 time(s)=22552
ep=020 tr=['train_clean_100'] loss=0.522 err=0.168 valid=dev_clean loss=0.644 err=0.177 lr_architecture1=0.000800 lr_architecture2=0.000200 time(s)=22484
ep=021 tr=['train_clean_100'] loss=0.518 err=0.167 valid=dev_clean loss=0.650 err=0.178 lr_architecture1=0.000800 lr_architecture2=0.000200 time(s)=22212
ep=022 tr=['train_clean_100'] loss=0.502 err=0.162 valid=dev_clean loss=0.647 err=0.176 lr_architecture1=0.000400 lr_architecture2=0.000100 time(s)=21969
ep=023 tr=['train_clean_100'] loss=0.496 err=0.161 valid=dev_clean loss=0.651 err=0.176 lr_architecture1=0.000400 lr_architecture2=0.000100 time(s)=21815
%WER 8.63 [ 4539 / 52576, 507 ins, 564 del, 3468 sub ] /scratch/ravanelm/pytorch-kaldi-new/exp/libri_LSTM_fmllr/decode_test_clean_out_dnn2/wer_17_0.5

