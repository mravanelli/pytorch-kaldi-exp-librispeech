ep=000 tr=['train_clean_100'] loss=1.749 err=0.418 valid=dev_clean loss=1.033 err=0.284 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=12207
ep=001 tr=['train_clean_100'] loss=1.082 err=0.298 valid=dev_clean loss=0.857 err=0.247 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=14357
ep=002 tr=['train_clean_100'] loss=0.904 err=0.262 valid=dev_clean loss=0.802 err=0.238 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=18811
ep=003 tr=['train_clean_100'] loss=0.832 err=0.245 valid=dev_clean loss=0.737 err=0.219 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=21400
ep=004 tr=['train_clean_100'] loss=0.796 err=0.237 valid=dev_clean loss=0.717 err=0.213 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=21388
ep=005 tr=['train_clean_100'] loss=0.770 err=0.231 valid=dev_clean loss=0.715 err=0.212 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=20938
ep=006 tr=['train_clean_100'] loss=0.750 err=0.226 valid=dev_clean loss=0.697 err=0.207 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=20670
ep=007 tr=['train_clean_100'] loss=0.733 err=0.222 valid=dev_clean loss=0.691 err=0.205 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=20764
ep=008 tr=['train_clean_100'] loss=0.720 err=0.218 valid=dev_clean loss=0.686 err=0.204 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=20902
ep=009 tr=['train_clean_100'] loss=0.707 err=0.215 valid=dev_clean loss=0.681 err=0.202 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=21036
ep=010 tr=['train_clean_100'] loss=0.696 err=0.213 valid=dev_clean loss=0.678 err=0.201 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=21322
ep=011 tr=['train_clean_100'] loss=0.686 err=0.210 valid=dev_clean loss=0.672 err=0.199 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=20931
ep=012 tr=['train_clean_100'] loss=0.678 err=0.208 valid=dev_clean loss=0.668 err=0.197 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=21002
ep=013 tr=['train_clean_100'] loss=0.670 err=0.206 valid=dev_clean loss=0.666 err=0.196 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=22411
ep=014 tr=['train_clean_100'] loss=0.663 err=0.204 valid=dev_clean loss=0.671 err=0.197 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=21063
ep=015 tr=['train_clean_100'] loss=0.623 err=0.194 valid=dev_clean loss=0.645 err=0.191 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=20995
ep=016 tr=['train_clean_100'] loss=0.610 err=0.191 valid=dev_clean loss=0.640 err=0.189 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=21276
ep=017 tr=['train_clean_100'] loss=0.603 err=0.189 valid=dev_clean loss=0.640 err=0.189 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=22307
ep=018 tr=['train_clean_100'] loss=0.597 err=0.187 valid=dev_clean loss=0.636 err=0.187 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=22626
ep=019 tr=['train_clean_100'] loss=0.592 err=0.186 valid=dev_clean loss=0.643 err=0.189 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=22114
ep=020 tr=['train_clean_100'] loss=0.573 err=0.181 valid=dev_clean loss=0.627 err=0.185 lr_architecture1=0.000100 lr_architecture2=0.000100 time(s)=22463
ep=021 tr=['train_clean_100'] loss=0.566 err=0.179 valid=dev_clean loss=0.630 err=0.185 lr_architecture1=0.000100 lr_architecture2=0.000100 time(s)=22260
ep=022 tr=['train_clean_100'] loss=0.556 err=0.176 valid=dev_clean loss=0.629 err=0.185 lr_architecture1=0.000050 lr_architecture2=0.000050 time(s)=22232
ep=023 tr=['train_clean_100'] loss=0.553 err=0.175 valid=dev_clean loss=0.625 err=0.183 lr_architecture1=0.000050 lr_architecture2=0.000050 time(s)=22377
%WER 8.66 [ 4551 / 52576, 521 ins, 556 del, 3474 sub ] /scratch/ravanelm/pytorch-kaldi-new/exp/libri_GRU_fmllr/decode_test_clean_out_dnn2/wer_13_0.5

